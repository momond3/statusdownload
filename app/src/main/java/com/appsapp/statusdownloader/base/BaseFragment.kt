package com.appsapp.statusdownloader.base

import androidx.lifecycle.ViewModel
import androidx.fragment.app.Fragment

abstract class BaseFragment<VM : ViewModel>: Fragment() {
    lateinit var mViewModel: VM
}
package com.appsapp.statusdownloader.application

import android.app.Application
import android.content.Context
import androidx.multidex.MultiDex
import com.appsapp.statusdownloader.BuildConfig
import com.appsapp.statusdownloader.di.viewModelModule
import com.facebook.ads.AudienceNetworkAds
import com.facebook.drawee.backends.pipeline.Fresco
import com.orhanobut.logger.AndroidLogAdapter
import com.orhanobut.logger.Logger
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class StatusDownloadApplication: Application() {
    override fun onCreate() {
        super.onCreate()
        initKoin()
        initLogger()
        initFresco()
        //  initAdMob()
        initFacebookAds()
    }

    private fun initFacebookAds() {
        AudienceNetworkAds.initialize(this)
    }

    private fun initAdMob() {
       // MobileAds.initialize(this, getString(R.string.admob_id))
    }

    private fun initFresco() {
        Fresco.initialize(this)
    }


    private fun initKoin() {
        startKoin {
            androidContext(this@StatusDownloadApplication)
            modules(viewModelModule)
        }
    }

    private fun initLogger() {
        Logger.addLogAdapter(object : AndroidLogAdapter() {
            override fun isLoggable(priority: Int, tag: String?): Boolean {
                return BuildConfig.DEBUG
            }
        })
    }


    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }


}
package com.appsapp.statusdownloader.extensions

import android.util.Log
import com.stfalcon.frescoimageviewer.ImageViewer
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlin.coroutines.CoroutineContext

val mainScope = CoroutineScope(Dispatchers.Main + SupervisorJob())
val bgScope = CoroutineScope(Dispatchers.IO + SupervisorJob())
val defaultScope = CoroutineScope(Dispatchers.Default + SupervisorJob())

val uiContext: CoroutineContext = Dispatchers.Main + CoroutineExceptionHandler { _, e ->
    Log.e("CommonsUtils uiContext", "CoroutineExceptionHandler", e)
}


val bgContext: CoroutineContext = Dispatchers.IO + CoroutineExceptionHandler { _, e ->
    Log.e("CommonsUtils bgContext", "CoroutineExceptionHandler", e)
}


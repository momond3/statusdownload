package com.appsapp.statusdownloader

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.appsapp.statusdownloader.databinding.ActivityMainBinding
import com.appsapp.statusdownloader.images.ImageListFragment
import com.appsapp.statusdownloader.settings.SettingsFragment
import com.appsapp.statusdownloader.videos.VideosListFragment
import com.facebook.ads.*
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.orhanobut.logger.Logger
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private val PLACEMENT_ID = "847708145628301_847709662294816"
    private lateinit var mInterticialAds: InterstitialAd

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_images -> {
                openFragment(ImageListFragment())
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_videos -> {
                openFragment(VideosListFragment())
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_settings -> {
                openFragment(SettingsFragment())
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        setupAds()
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
    }

    private fun setupAds() {
        mInterticialAds = InterstitialAd(this, PLACEMENT_ID)
        mInterticialAds.setAdListener(object : InterstitialAdListener {

            override fun onInterstitialDisplayed(p0: Ad?) {
               Logger.e("onInterstitialDisplayed")
            }

            override fun onAdClicked(p0: Ad?) {
                Logger.e("onAdClicked")
            }

            override fun onInterstitialDismissed(p0: Ad?) {
                Logger.e("onInterstitialDismissed")
                binding.homeActivityProgress.visibility = View.GONE
                binding.imageContainer.visibility = View.VISIBLE
                binding.navigation.visibility = View.VISIBLE
                openFragment(ImageListFragment())
            }

            override fun onError(p0: Ad?, p1: AdError?) {
                Logger.e("onInterstitial Error ${p1?.errorMessage}")
                binding.homeActivityProgress.visibility = View.GONE
                binding.imageContainer.visibility = View.VISIBLE
                binding.navigation.visibility = View.VISIBLE
                openFragment(ImageListFragment())
            }

            override fun onAdLoaded(p0: Ad?) {
                Logger.e("onAdLoaded")
                mInterticialAds.show()
            }

            override fun onLoggingImpression(p0: Ad?) {
                Logger.e("onLoggingImpression")
            }
        })
        AdSettings.addTestDevice("f28b60d7-2fef-472b-9b58-3e8595c96423")
        mInterticialAds.loadAd()

    }

    override fun onDestroy() {
        mInterticialAds.destroy()
        super.onDestroy()
    }

    private fun openFragment(fragment: Fragment) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.image_container, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount == 1) {
            val dialog = AlertDialog.Builder(this@MainActivity)
            dialog.setTitle(R.string.exit)
                .setMessage(R.string.you_want_exit)
                .setPositiveButton(R.string.exit) { dialogInterface, i ->
                    moveTaskToBack(true)
                    android.os.Process.killProcess(android.os.Process.myPid())
                    System.exit(0)
                }
                .setNegativeButton(R.string.cancel) { dialogInterface, i -> dialogInterface.dismiss() }
            dialog.show()

        } else super.onBackPressed()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item?.itemId) {
            R.id.app_bar_refresh -> {}
        }
        return true
    }
}

package com.appsapp.statusdownloader.utils

import android.annotation.SuppressLint
import android.graphics.Bitmap
import android.media.ThumbnailUtils
import android.provider.MediaStore
import com.orhanobut.logger.Logger
import java.text.SimpleDateFormat
import java.util.*

object SaverUtils {

    @SuppressLint("SimpleDateFormat")
    fun getCurrentDate(): String {
        val formatter = SimpleDateFormat("dd-MM-yyyy")
        return formatter.format(Date())
    }


    fun formatDateWithTimeStamp(timeStamp: Long?): String {
        val date = Date(timeStamp?: 0L)
        val formatter = SimpleDateFormat("yyyy-MM-dd HH:mm")
        return formatter.format(date)
    }


    fun getThumbnailsFromVideo(path: String): Bitmap? = ThumbnailUtils.createVideoThumbnail(path, MediaStore.Images.Thumbnails.MINI_KIND)
}
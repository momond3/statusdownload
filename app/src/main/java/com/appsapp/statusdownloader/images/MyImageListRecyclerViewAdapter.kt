package com.appsapp.statusdownloader.images


import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.appsapp.statusdownloader.R
import com.appsapp.statusdownloader.databinding.ItemImageListBinding
import com.appsapp.statusdownloader.entities.Image
import com.appsapp.statusdownloader.utils.SaverUtils
import com.orhanobut.logger.Logger
import java.io.File


class MyImageListRecyclerViewAdapter(private var mValues: MutableList<Image>?, private var itemClick: (Image, Int) -> Unit) : RecyclerView.Adapter<MyImageListRecyclerViewAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_image_list, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = mValues!![position]
        holder.itemBinding?.image?.setImageURI(Uri.fromFile(File(item.path)))
        holder.itemBinding?.root?.setOnClickListener { itemClick(item, position) }
    }

    override fun getItemCount(): Int = mValues?.size ?: 0

    fun setData(list: MutableList<Image>) {
        mValues = list
        notifyDataSetChanged()
    }

    inner class ViewHolder(mView: View) : RecyclerView.ViewHolder(mView) {
        internal var itemBinding: ItemImageListBinding? = null

        init {
            itemBinding = DataBindingUtil.bind(mView)
        }
    }
}

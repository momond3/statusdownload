package com.appsapp.statusdownloader.images

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.LinearLayout
import android.widget.RelativeLayout
import com.appsapp.statusdownloader.R
import com.facebook.ads.*
import com.github.clans.fab.FloatingActionButton
import com.github.clans.fab.FloatingActionMenu
import com.orhanobut.logger.Logger

class ImageOverlayView(ctx: Context, attrs: AttributeSet? = null, defStyle: Int = 0): RelativeLayout(ctx, attrs, defStyle) {
    private lateinit var optionButton: FloatingActionMenu
    lateinit var saveButton: FloatingActionButton
    lateinit var shareButton: FloatingActionButton
    lateinit var repostButton: FloatingActionButton
    lateinit var deleteButton: FloatingActionButton
    lateinit var adContainer: LinearLayout
    private val PLACEMENT_ID = "847708145628301_848403232225459"
    private lateinit var mBannerAdView: AdView



    init { initView() }

    private fun initView() {
        val view: View = View.inflate(context, R.layout.view_image_overlay, this)
        optionButton = view.findViewById(R.id.options)
        saveButton = view.findViewById(R.id.save_image)
        shareButton = view.findViewById(R.id.share_image)
        repostButton = view.findViewById(R.id.repost_image)
        deleteButton = view.findViewById(R.id.remove_image)
        adContainer = view.findViewById(R.id.overlay_baner_ads)

        mBannerAdView = AdView(view.context, PLACEMENT_ID, AdSize.BANNER_HEIGHT_50)
        adContainer.addView(mBannerAdView)
        AdSettings.addTestDevice("f28b60d7-2fef-472b-9b58-3e8595c96423")
        mBannerAdView.loadAd()
        mBannerAdView.setAdListener(object : AdListener {
            override fun onAdClicked(p0: Ad?) {
                Logger.e("onAdClicked")
            }

            override fun onError(p0: Ad?, p1: AdError?) {
                Logger.e("onError ${p1?.errorMessage}")
            }

            override fun onAdLoaded(p0: Ad?) {
                Logger.e("onAdLoaded ")
            }

            override fun onLoggingImpression(p0: Ad?) {
                Logger.e("onLoggingImpression")
            }

        })
    }

}
package com.appsapp.statusdownloader.images

import android.Manifest
import android.annotation.SuppressLint
import android.app.Application
import android.content.ContentValues
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.content.FileProvider
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.appsapp.statusdownloader.BuildConfig
import com.appsapp.statusdownloader.R
import com.appsapp.statusdownloader.base.BaseFragment
import com.appsapp.statusdownloader.databinding.FragmentImagelistListBinding
import com.appsapp.statusdownloader.entities.Image
import com.appsapp.statusdownloader.utils.SaverUtils
import com.facebook.ads.*
import com.orhanobut.logger.Logger
import com.stfalcon.frescoimageviewer.ImageViewer
import com.tbruyelle.rxpermissions2.RxPermissions
import org.apache.commons.io.FileUtils
import org.koin.androidx.viewmodel.ext.android.getViewModel
import java.io.File
import java.io.IOException
import java.util.*


class ImageListFragment : BaseFragment<ImageViewModel>() {

    private lateinit var binding: FragmentImagelistListBinding
    private lateinit var imageAdapter: MyImageListRecyclerViewAdapter
    private var imageList: MutableList<Image> = mutableListOf()
    private var currentImagePosition: Int = 0
    private lateinit var mBannerAdView: AdView
    private val BANNERPLACEMENTID = "847708145628301_848396185559497"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mViewModel = getViewModel()
    }

    @SuppressLint("CheckResult")
    private fun observeViewModel() {
        val permission = RxPermissions(this)
        permission.request(
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE)
            .subscribe { granted ->
                if (granted) {
                    mViewModel.getImages().observe(this, Observer {
                        it?.let { list ->
                            Logger.e("List change ${list.size}")
                            imageList = it
                            imageAdapter.setData(imageList)
                            binding.imageListRefresh.isRefreshing = false
                            if (imageList.isEmpty()) {
                                binding.emptyImages.visibility = View.VISIBLE
                                binding.imageList.visibility = View.GONE
                            } else  {
                                binding.emptyImages.visibility = View.GONE
                                binding.imageList.visibility = View.VISIBLE
                            }
                        }
                    })
                }
            }

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_imagelist_list, container, false)
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupAds()
        setupRecyclerView()
        observeViewModel()
        setupRefresh()
    }

    private fun setupRefresh() {
        binding.imageListRefresh.apply {
            setColorSchemeResources(R.color.colorPrimary,R.color.sw1, R.color.sw2, R.color.sw3 )
            setOnRefreshListener {
                observeViewModel()
            }
        }
    }


    private fun setupAds() {
        mBannerAdView = AdView(context, BANNERPLACEMENTID, AdSize.BANNER_HEIGHT_50)
        val adContainer = binding.imageListBannerContainer
        adContainer.addView(mBannerAdView)
        AdSettings.addTestDevice("f28b60d7-2fef-472b-9b58-3e8595c96423")
        mBannerAdView.loadAd()
        mBannerAdView.setAdListener(object : AdListener {
            override fun onAdClicked(p0: Ad?) {
                Logger.e("onAdClicked")
            }

            override fun onError(p0: Ad?, p1: AdError?) {
                Logger.e("onError ${p1?.errorMessage}")
            }

            override fun onAdLoaded(p0: Ad?) {
                Logger.e("onAdLoaded ")
            }

            override fun onLoggingImpression(p0: Ad?) {
                Logger.e("onLoggingImpression")
            }

        })

    }


    private fun setupRecyclerView() {
        imageAdapter = MyImageListRecyclerViewAdapter(imageList) { image, position -> onImageClicked(image, position) }
        binding.imageList.adapter = imageAdapter
        binding.imageList.layoutManager = GridLayoutManager(context, 2)
    }

    override fun onDestroy() {
        mBannerAdView.destroy()
        super.onDestroy()

    }


    private fun onImageClicked(img: Image, position: Int) {
        currentImagePosition = position

        val overlayImage = ImageOverlayView(context!!)
        val imageViewer = ImageViewer.Builder(context, imageList)
        imageViewer.setFormatter { Uri.fromFile(File(it.path)).toString() }
            .hideStatusBar(false)
            .allowSwipeToDismiss(true)
            .allowZooming(true)
            .hideStatusBar(false)
            .setStartPosition(currentImagePosition)
            .setOverlayView(overlayImage)
            .setContainerPaddingPx( 0, 0, 0, 200)
            .setImageChangeListener(getImageChangeListener())
            .show()

        overlayImage.apply {
            saveButton.setOnClickListener {
                val path = mViewModel.saveImage(imageList[currentImagePosition])

                File(path).let {
                    Logger.e("Local File name ${it.name} and r=${it.canRead()} e= ${it.canExecute()} and w=${it.canWrite()} h=${it.isHidden}")

                }

            }

            shareButton.setOnClickListener {
                val currentImage = imageList[currentImagePosition]
                if (File(currentImage.path).exists()) {
                    val fileUri = FileProvider.getUriForFile(
                        context,
                        BuildConfig.FILE_PROVIDER,
                        File(currentImage.path)
                    )
                    val shareIntent = Intent()
                    shareIntent.action = Intent.ACTION_SEND
                    shareIntent.putExtra(Intent.EXTRA_STREAM, fileUri)
                    shareIntent.type = "image/*"
                    shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                    startActivity(Intent.createChooser(shareIntent, getString(R.string.share)))
                }
            }

            repostButton.setOnClickListener {
                val currentImage = imageList[currentImagePosition]
                if (File(currentImage.path).exists()) {
                    val fileUri = FileProvider.getUriForFile(
                        context,
                        BuildConfig.FILE_PROVIDER
                        ,
                        File(currentImage.path)
                    )
                    val shareIntent = Intent()
                    shareIntent.action = Intent.ACTION_SEND
                    shareIntent.putExtra(Intent.EXTRA_STREAM, fileUri)
                    shareIntent.type = "image/*"
                    shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                    shareIntent.`package` = "com.whatsapp"
                    startActivity(Intent.createChooser(shareIntent, getString(R.string.share)))
                }
            }

            deleteButton.setOnClickListener {
                val dialog = AlertDialog.Builder(context)
                dialog.setTitle(getString(R.string.delete_file))
                    .setMessage(getString(R.string.deletion_confirmation))
                    .setNegativeButton(getString(R.string.cancel)) { dialogInterface, _ ->
                        dialogInterface.dismiss()
                    }
                    .setPositiveButton(getString(R.string.delete)) { dialogInterface, _ ->
                        val currentImage = imageList[currentImagePosition]
                        mViewModel.deleteImage(currentImage)
                        imageList.remove(currentImage)
                        imageAdapter.setData(imageList)
                        dialogInterface.dismiss()
                        imageViewer.dismiss()
                    }

                dialog.show()
            }

        }
    }

    private fun getImageChangeListener(): ImageViewer.OnImageChangeListener {
        return ImageViewer.OnImageChangeListener { position ->
            currentImagePosition = position
        }
    }
}


class ImageViewModel(val app: Application) : AndroidViewModel(app) {

    fun getImages(): LiveData<MutableList<Image>> {
        val imageList: MutableLiveData<MutableList<Image>> = MutableLiveData()
        val list: MutableList<Image> = mutableListOf()
        val folderPath = Environment.getExternalStorageDirectory().toString() + "/WhatsApp/Media/.Statuses"

        val directory = File(folderPath)
        if (directory.exists()) {
            val images = directory.listFiles().filter { it.name.contains(".jpg").or(it.name.contains(".png")) }
                .sortedByDescending { it.lastModified() }
            if (images.isNotEmpty())
                images.forEach {
                    list.add(Image(it.name, it.path, it.lastModified()))
                }
        }
        imageList.value = list
        return imageList
    }


    fun saveImage(img: Image): String {

        val publicFolder = File(Environment.getExternalStorageDirectory(), "StatusSaver")

        if (!publicFolder.exists()) {
            publicFolder.mkdir()
        }

        val sourceFile = File(img.path)


        try {
            FileUtils.copyFileToDirectory(sourceFile, publicFolder, false)

            val copiedFile = File("$publicFolder/${sourceFile.name}")
            val newFileName = File("$publicFolder/IMG_${SaverUtils.getCurrentDate()}_${Date().time}.jpg")
            copiedFile.renameTo(newFileName)

            val values = ContentValues()
            values.put(MediaStore.Images.Media.DATE_TAKEN, System.currentTimeMillis())
            values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpg")
            values.put(MediaStore.MediaColumns.DATA, "$publicFolder/${newFileName.name}")
            app.contentResolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values)
        } catch (e: IOException) {
            e.printStackTrace()
            Toast.makeText(
                app.applicationContext,
                app.applicationContext.getString(R.string.error_copy),
                Toast.LENGTH_LONG
            ).show()
        } finally {
            Toast.makeText(
                app.applicationContext,
                app.applicationContext.getString(R.string.image_saved_successful),
                Toast.LENGTH_LONG
            ).show()
        }

        return publicFolder.name

    }

    fun deleteImage(img: Image) {
        FileUtils.forceDelete(File(img.path))
        Toast.makeText(
            app.applicationContext,
            app.applicationContext.getString(R.string.file_delete_success),
            Toast.LENGTH_LONG).show()
    }

}
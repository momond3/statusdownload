package com.appsapp.statusdownloader.settings


import android.app.ProgressDialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.appsapp.statusdownloader.BuildConfig
import com.appsapp.statusdownloader.R
import com.appsapp.statusdownloader.databinding.FragmentSettingsBinding
import com.crashlytics.android.Crashlytics
import com.google.firebase.dynamiclinks.DynamicLink
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks
import com.orhanobut.logger.Logger
import com.github.javiersantos.materialstyleddialogs.MaterialStyledDialog





/**
 * A simple [Fragment] subclass.
 *
 */
class SettingsFragment : Fragment() {
    private lateinit var settingsBinding: FragmentSettingsBinding
    private val playstoreLink = "https://play.google.com/store/apps/details?id=${BuildConfig.APPLICATION_ID}"

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        settingsBinding = DataBindingUtil.inflate(inflater,R.layout.fragment_settings, container, false )
        return settingsBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        handleClicks()
    }


    private fun handleClicks() {
        settingsBinding.apply {
            feedbackLayout.setOnClickListener {
                val emailIntent = Intent(Intent.ACTION_SENDTO)
                emailIntent.data = Uri.parse("mailto:devsncontact@gmail.com")
                startActivity(Intent.createChooser(emailIntent, getString(R.string.send_feedback)))
            }

            rateLayout.setOnClickListener {
                val appPackageName = context?.packageName // getPackageName() from Context or Activity object
                try {
                    startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=$appPackageName")))
                } catch (anfe: android.content.ActivityNotFoundException) {
                    startActivity(
                        Intent(
                            Intent.ACTION_VIEW,
                            Uri.parse("https://play.google.com/store/apps/details?id=$appPackageName")
                        )
                    )
                }
            }

            shareLayout.setOnClickListener {
                shareApplication()
            }


            versionLayout.setOnClickListener {
                MaterialStyledDialog.Builder(context)
                    .setTitle(getString(R.string.app_version))
                    .setIcon(R.drawable.ic_version_white)
                    .setDescription(getString(R.string.version_desc, BuildConfig.VERSION_NAME))
                    .show()
            }

        }
    }


    private fun shareApplication() {
        val progressDialog = ProgressDialog(context)
        progressDialog.apply {
            setTitle(getString(R.string.please_wait))
        }
        progressDialog.show()
        FirebaseDynamicLinks.getInstance().createDynamicLink()
            .setLink(Uri.parse(playstoreLink))
            .setDomainUriPrefix("https://appsapp.page.link")
            .setAndroidParameters(DynamicLink.AndroidParameters.Builder().build())
            .buildShortDynamicLink()
            .addOnSuccessListener {
                progressDialog.dismiss()
                shareByIntent(it.shortLink.toString())
            }
            .addOnFailureListener {
                progressDialog.dismiss()
                Logger.e("Error creating dynamic link ${it.message}")
            }
    }


    private fun shareByIntent(link: String) {
        val sendIntent = Intent()
        sendIntent.action = Intent.ACTION_SEND
        sendIntent.putExtra(Intent.EXTRA_TEXT,
            getString(R.string.share_app_link,link))
        sendIntent.type = "text/plain"
        startActivity(Intent.createChooser(sendIntent, resources.getString(R.string.share)))
    }
}
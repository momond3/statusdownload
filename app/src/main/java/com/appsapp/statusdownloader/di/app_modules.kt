package com.appsapp.statusdownloader.di

import com.appsapp.statusdownloader.images.ImageViewModel
import com.appsapp.statusdownloader.videos.VideoViewModel
import org.koin.android.ext.koin.androidApplication
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module


var viewModelModule = module {
    viewModel { ImageViewModel(androidApplication()) }
    viewModel { VideoViewModel(androidApplication()) }
}

package com.appsapp.statusdownloader.events

import com.squareup.otto.Bus


object BusProvider {
    @get:Synchronized
    val instance: Bus = MainThreadBus()
}
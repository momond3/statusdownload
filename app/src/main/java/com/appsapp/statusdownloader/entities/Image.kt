package com.appsapp.statusdownloader.entities

data class Image(val name: String?, val path: String?, val date: Long?)
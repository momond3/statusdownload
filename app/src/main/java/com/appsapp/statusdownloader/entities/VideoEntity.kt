package com.appsapp.statusdownloader.entities

import android.graphics.Bitmap
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class VideoEntity(val name: String?, val path: String?, val date: Long?, var thumb: Bitmap?): Parcelable
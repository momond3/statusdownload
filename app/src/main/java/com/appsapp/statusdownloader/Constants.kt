package com.appsapp.statusdownloader

object Constants {
    const val VIDEO_OBJECT_KEY = "video_object"
    const val VIDEO_PATH_KEY = "video_path"
    const val VIDEO_THUMB_KEY = "video_thumb"
    const val POSITION_KEY = "position"

}
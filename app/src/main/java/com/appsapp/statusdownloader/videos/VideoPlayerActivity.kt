package com.appsapp.statusdownloader.videos

import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.FileProvider
import androidx.databinding.DataBindingUtil
import com.appsapp.statusdownloader.BuildConfig
import com.appsapp.statusdownloader.Constants
import com.appsapp.statusdownloader.R
import com.appsapp.statusdownloader.databinding.ActivityVideoPlayerBinding
import com.appsapp.statusdownloader.events.BusProvider
import com.appsapp.statusdownloader.events.VideoDeletedEvent
import com.facebook.ads.*
import com.orhanobut.logger.Logger
import fm.jiecao.jcvideoplayer_lib.JCVideoPlayer
import org.koin.androidx.viewmodel.ext.android.getViewModel
import java.io.File

class VideoPlayerActivity : AppCompatActivity() {

    private lateinit var binding: ActivityVideoPlayerBinding
    private var mViewModel: VideoViewModel? = null
    private var path: String? = null
    private val PLACEMENT_ID = "847708145628301_848411172224665"
    private lateinit var mBannerAdView: AdView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_video_player)
        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        path = intent?.getStringExtra(Constants.VIDEO_PATH_KEY)!!
        mViewModel = getViewModel()
        setupAds()
        setupPlayer()
        handleClicks()
    }

    private fun setupAds() {
        mBannerAdView = AdView(this, PLACEMENT_ID, AdSize.BANNER_HEIGHT_50)
        val adContainer = binding.contentVideoPlayer.viewImageOverlay.overlayBanerAds
        adContainer.addView(mBannerAdView)
        AdSettings.addTestDevice("f28b60d7-2fef-472b-9b58-3e8595c96423")
        mBannerAdView.loadAd()
        mBannerAdView.setAdListener(object : AdListener {
            override fun onAdClicked(p0: Ad?) {
                Logger.e("onAdClicked")
            }

            override fun onError(p0: Ad?, p1: AdError?) {
                Logger.e("onError ${p1?.errorMessage}")
            }

            override fun onAdLoaded(p0: Ad?) {
                Logger.e("onAdLoaded ")
            }

            override fun onLoggingImpression(p0: Ad?) {
                Logger.e("onLoggingImpression")
            }

        })
    }


    private fun handleClicks() {
        binding.contentVideoPlayer.viewImageOverlay.apply {
            shareImage.setOnClickListener {
                if (File(path).exists()) {
                    val fileUri = FileProvider.getUriForFile(
                        this@VideoPlayerActivity,
                        BuildConfig.FILE_PROVIDER,
                        File(path)
                    )

                    val shareIntent = Intent()
                    shareIntent.action = Intent.ACTION_SEND
                    shareIntent.putExtra(Intent.EXTRA_STREAM, fileUri)
                    shareIntent.type = "video/*"
                    shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                    startActivity(Intent.createChooser(shareIntent, getString(R.string.share)))
                }
            }

            saveImage.setOnClickListener {
                path?.let {
                    mViewModel?.saveVideo(it)
                }
            }

            repostImage.setOnClickListener {
                if (File(path).exists()) {
                    val fileUri = FileProvider.getUriForFile(
                        this@VideoPlayerActivity,
                        BuildConfig.FILE_PROVIDER,
                        File(path)
                    )
                    val shareIntent = Intent()
                    shareIntent.action = Intent.ACTION_SEND
                    shareIntent.putExtra(Intent.EXTRA_STREAM, fileUri)
                    shareIntent.type = "video/*"
                    shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                    shareIntent.`package` = "com.whatsapp"
                    startActivity(Intent.createChooser(shareIntent, getString(R.string.share)))
                }
            }

            removeImage.setOnClickListener { deleteVideo() }
        }
    }

    private fun deleteVideo() {
        val dialog = AlertDialog.Builder(this@VideoPlayerActivity).apply {
            title = getString(R.string.delete_file)
            setMessage(getString(R.string.deletion_confirmation))
            setNegativeButton(getString(R.string.cancel)) { dialogInterface: DialogInterface, _: Int ->
                dialogInterface.dismiss()
            }
            setPositiveButton(getString(R.string.delete)) { dialogInterface, _ ->
                mViewModel?.deleteVideo(path!!)
                BusProvider.instance.post(VideoDeletedEvent(intent?.getIntExtra(Constants.POSITION_KEY, -1)!!))
                dialogInterface.dismiss()
                finish()
            }
        }
        dialog.show()
    }

    override fun onDestroy() {
        mBannerAdView.destroy()
        super.onDestroy()
    }
    private fun setupPlayer() {
        path?.let { v ->
            binding.contentVideoPlayer.videoPlayer.apply {
                setUp(v, "")
            }
        } ?: run { Toast.makeText(this@VideoPlayerActivity, "Erreur lors de la lecture", Toast.LENGTH_LONG).show() }
    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> onBackPressed()
        }
        return true
    }


    override fun onPause() {
        super.onPause()
        JCVideoPlayer.releaseAllVideos()
    }
}

package com.appsapp.statusdownloader.videos


import android.Manifest
import android.annotation.SuppressLint
import android.app.Application
import android.content.ContentValues
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import com.appsapp.statusdownloader.Constants
import com.appsapp.statusdownloader.R
import com.appsapp.statusdownloader.base.BaseFragment
import com.appsapp.statusdownloader.databinding.FragmentVideosListBinding
import com.appsapp.statusdownloader.entities.VideoEntity
import com.appsapp.statusdownloader.events.BusProvider
import com.appsapp.statusdownloader.events.VideoDeletedEvent
import com.appsapp.statusdownloader.extensions.bgContext
import com.appsapp.statusdownloader.extensions.mainScope
import com.appsapp.statusdownloader.utils.SaverUtils
import com.facebook.ads.*
import com.orhanobut.logger.Logger
import com.squareup.otto.Subscribe
import com.tbruyelle.rxpermissions2.RxPermissions
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.apache.commons.io.FileUtils
import org.koin.androidx.viewmodel.ext.android.getViewModel
import java.io.File
import java.io.IOException
import java.util.*


/**
 * A simple [Fragment] subclass.
 *
 */
class VideosListFragment : BaseFragment<VideoViewModel>() {
    private lateinit var videosBinding: FragmentVideosListBinding
    private lateinit var videosAdapter: VideosAdapter
    private var videosList: MutableList<VideoEntity> = mutableListOf()
    private lateinit var mBannerAdView: AdView
    private val PLACEMENT_ID = "847708145628301_848401395558976"


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mViewModel = getViewModel()
    }


    @SuppressLint("CheckResult")
    private fun observeViewModel() {
        val permission = RxPermissions(this)
        permission.request(
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        ).subscribe { granted ->
            if (granted) {
                mViewModel.getVideos().observe(this@VideosListFragment, Observer {
                    it?.let { list ->
                        Logger.e("List videos change ${list.size}")
                        videosBinding.videoListLoader.visibility = View.GONE
                        videosList = it
                        videosAdapter.setData(videosList)
                        createThumbnailByVideo()

                        if (videosList.isEmpty()) {
                            videosBinding.emptyVideos.visibility = View.VISIBLE
                            videosBinding.videosList.visibility = View.GONE
                        } else {
                            videosBinding.emptyVideos.visibility = View.GONE
                            videosBinding.videosList.visibility = View.VISIBLE
                        }
                    }
                })

            }
        }

    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        videosBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_videos_list, container, false)
        BusProvider.instance.register(this)
        return videosBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupAds()
        setupAdapter()
        observeViewModel()
    }


    private fun setupAds() {
        mBannerAdView = AdView(context, PLACEMENT_ID, AdSize.BANNER_HEIGHT_50)
        val adContainer = videosBinding.videoListBannerContainer
        adContainer.addView(mBannerAdView)
        AdSettings.addTestDevice("f28b60d7-2fef-472b-9b58-3e8595c96423")
        mBannerAdView.loadAd()
        mBannerAdView.setAdListener(object : AdListener {
            override fun onAdClicked(p0: Ad?) {
                Logger.e("onAdClicked")
            }

            override fun onError(p0: Ad?, p1: AdError?) {
                Logger.e("onError ${p1?.errorMessage}")
            }

            override fun onAdLoaded(p0: Ad?) {
                Logger.e("onAdLoaded ")
            }

            override fun onLoggingImpression(p0: Ad?) {
                Logger.e("onLoggingImpression")
            }

        })
    }


    private fun setupAdapter() {
        videosAdapter = VideosAdapter(videosList) { video, i -> videoClicked(video, i) }
        videosBinding.videosList.adapter = videosAdapter
        videosBinding.videosList.layoutManager = GridLayoutManager(context, 2)
    }


    private fun videoClicked(v: VideoEntity, position: Int) {
        val intent = Intent(activity!!, VideoPlayerActivity::class.java).apply {
            putExtra(Constants.VIDEO_PATH_KEY, v.path)
            putExtra(Constants.POSITION_KEY, position)
        }
        startActivity(intent)
    }


    @Subscribe
    fun onVideoRemoved(ev: VideoDeletedEvent) {
        Logger.e("Video to delete ${ev.posittion}")
        videosList.removeAt(ev.posittion)
        videosAdapter.setData(videosList)
    }


    override fun onDestroyView() {
        super.onDestroyView()
        BusProvider.instance.unregister(this)
    }


    private fun createThumbnailByVideo() {

        videosList.forEachIndexed { index, videoEntity ->
            mainScope.launch {
                val th: Bitmap? = withContext(bgContext) { SaverUtils.getThumbnailsFromVideo(videoEntity.path!!) }
                videosAdapter.setThumb(th!!, index)
            }
        }
    }
}


class VideoViewModel(val app: Application) : AndroidViewModel(app) {

    fun getVideos(): LiveData<MutableList<VideoEntity>> {
        val videoList: MutableLiveData<MutableList<VideoEntity>> = MutableLiveData()
        val list: MutableList<VideoEntity> = mutableListOf()

        mainScope.launch {
            val folderPath = Environment.getExternalStorageDirectory().toString() + "/WhatsApp/Media/.Statuses"
            val directory = File(folderPath)
            if (directory.exists()) {
                val videos =
                    directory.listFiles().filter { it.name.endsWith(".mp4") }.sortedByDescending { it.lastModified() }
                if (videos.isNotEmpty()) {
                    videos.forEach { file ->
                        list.add(VideoEntity(file.name, file.path, file.lastModified(), null))
                    }
                }
                videoList.value = list
            }
        }

        return videoList
    }


    fun saveVideo(videoPath: String): String {
        val publicFolder = File(Environment.getExternalStorageDirectory(), "StatusSaver")

        if (!publicFolder.exists()) {
            publicFolder.mkdir()
        }

        val sourceFile = File(videoPath)


        try {
            FileUtils.copyFileToDirectory(sourceFile, publicFolder, false)

            val copiedFile = File("$publicFolder/${sourceFile.name}")
            val newFileName = File("$publicFolder/VIDEO_${SaverUtils.getCurrentDate()}_${Date().time}.mp4")
            copiedFile.renameTo(newFileName)

            val values = ContentValues()
            values.put(MediaStore.Video.Media.DATE_TAKEN, System.currentTimeMillis())
            values.put(MediaStore.Video.Media.MIME_TYPE, "video/mp4")
            values.put(MediaStore.MediaColumns.DATA, "$publicFolder/${newFileName.name}")
            app.contentResolver.insert(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, values)
        } catch (e: IOException) {
            e.printStackTrace()
            Toast.makeText(
                app.applicationContext,
                app.applicationContext.getString(R.string.error_copy),
                Toast.LENGTH_LONG
            ).show()
        } finally {
            Toast.makeText(
                app.applicationContext,
                app.applicationContext.getString(R.string.video_saved_successful),
                Toast.LENGTH_LONG
            ).show()
        }

        return publicFolder.name
    }


    fun deleteVideo(videoPath: String) {
        FileUtils.forceDelete(File(videoPath))
        Toast.makeText(
            app.applicationContext,
            app.applicationContext.getString(R.string.file_delete_success),
            Toast.LENGTH_LONG
        ).show()
    }
}
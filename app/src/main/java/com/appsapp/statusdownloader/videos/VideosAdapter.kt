package com.appsapp.statusdownloader.videos

import android.graphics.Bitmap
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.appsapp.statusdownloader.R
import com.appsapp.statusdownloader.databinding.ItemVideoBinding
import com.appsapp.statusdownloader.entities.VideoEntity
import com.appsapp.statusdownloader.extensions.bgContext
import com.appsapp.statusdownloader.extensions.mainScope
import com.appsapp.statusdownloader.utils.SaverUtils
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class VideosAdapter(var videosList: MutableList<VideoEntity>?, private var videosCLick: (VideoEntity, Int) -> Unit): RecyclerView.Adapter<VideosAdapter.VideosViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VideosViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_video, parent, false)
        return VideosViewHolder(view)
    }

    override fun getItemCount(): Int = videosList?.size ?: 0

    override fun onBindViewHolder(holder: VideosViewHolder, position: Int) {
        val v = videosList!![position]

        holder.itemVideo?.let { view ->
            v.thumb?.let { view.video.setImageBitmap(v.thumb) }
            view.root.setOnClickListener { videosCLick(v, position) }
        }
    }


    fun setData(l: MutableList<VideoEntity>) {
        videosList = l
        notifyDataSetChanged()
    }


    fun setThumb(th: Bitmap, position: Int) {
        val item = videosList!![position]
        item.thumb = th
        notifyItemChanged(position)
    }


    class VideosViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        var itemVideo: ItemVideoBinding? = null

        init {
            itemVideo = DataBindingUtil.bind(itemView)
        }
    }
}
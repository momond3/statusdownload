
    fun saveImage(img: Image, ctc: Context): String {
        val publicFolder = File(Environment.getExternalStorageDirectory(), "StatusSaver")

        if (!publicFolder.exists()) {
            publicFolder.mkdir()
        }

        val sourceFile = File(img.path)

        val destFile = File(publicFolder.path, "IMAGE1.jpg")

        if (!destFile.exists()) {
            destFile.createNewFile()
        }

        try {
            FileUtils.copyFileToDirectory(sourceFile, publicFolder)

            val values = ContentValues()
            values.put(MediaStore.Images.Media.DATE_TAKEN, System.currentTimeMillis())
            values.put(MediaStore.Images.Media.MIME_TYPE, "image/*")
            values.put(MediaStore.MediaColumns.DATA, "$destFile/${sourceFile.name}")
            ctc.contentResolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values)
        } catch (e: IOException) {
            Logger.e("Erreur lors de la copie ${e.printStackTrace()}")
        } finally {

            Logger.e("SAved with sucess ${destFile.path}")
        }

        return  destFile.path
    }
